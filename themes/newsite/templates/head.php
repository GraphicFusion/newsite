<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<!--      ________                     .__    .__         ___________           .__             -->
<!--     /  _____/___________  ______ |  |__ |__| ____   \_   _____/_ __  _____|__| ____   ____-->
<!--    /   \  __\_  __ \__  \ \____ \|  |  \|  |/ ___\   |    __)|  |  \/  ___/  |/  _ \ /    \-->
<!--    \    \_\  \  | \// __ \|  |_> >   Y  \  \  \___   |     \ |  |  /\___ \|  (  <_> )   |  \-->
<!--     \______  /__|  (____  /   __/|___|  /__|\___  >  \___  / |____//____  >__|\____/|___|  /-->
<!--            \/           \/|__|        \/        \/       \/             \/               \/-->
<!-----Crafted with pride by Graphic Fusion Design Team >  http://graphicfusiondesign.com -2014---->
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title('|', true, 'right'); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); ?>

  <link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/assets/img/favicon.ico'; ?>" type="image/x-icon">
  <link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
</head>
