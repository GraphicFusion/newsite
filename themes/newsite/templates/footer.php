<footer class="content-info" role="contentinfo">
  <div class="container">
      <?php  if (has_nav_menu('footer-menu')) :
          wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'nav navbar-right ' ) );
      endif;?>

    <?php dynamic_sidebar('sidebar-footer'); ?>
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> <span class="gfdcopy">, Site Design by <a href="http://graphicfusiondesign.com" title="Tucson Web design">Graphic Fusion</a></span></p>
  </div>
</footer>

<?php wp_footer(); ?>
