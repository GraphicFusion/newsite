<?php
/**
 * Created by Alex Gurevich.
 * Will create a banner image at the top using post's featured image, if no image nothing will be displayed
 * remove inline css as needed
 * Date: 6/12/14
 * Time: 10:14 AM
 */

?>

<?php if (has_post_thumbnail( $post->ID ) ): ?>
    <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
    <div class="top-banner" style="background-image: url('<?php echo $image[0]; ?>'); background-size: cover; display: block; width: 100%; height: 100%; min-height: 400px;">

    </div>
<?php endif; ?>