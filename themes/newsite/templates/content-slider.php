<?php while (have_posts()) : the_post();
	/**
	 * To use slider, set up Advanced Custom Fields with:
	 * 1. A field called slides that is a repeater
	 * 2. A field called image under slides that is an image type of field
	 * 3. A field called caption under slides that is a text type of field
	 * 4. Optionally a field for a link under slides. You'll need to add the link where ever you need it to the html below too
	 */
	?>
	<!-- FRONT PAGE SLIDER -->
	<?php if (have_rows('slides')) : ?>
	<div id="carousel-frontpage" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <?php $count = 0; ?>
	  <ol class="carousel-indicators">
	  <?php while( have_rows('slides') ): the_row(); ?>
	  	<?php if ($count == 0) $active = " class='active'"; ?>
	    <li data-target="#carousel-frontpage" data-slide-to="<?php echo $count; ?>"<?php echo $active;?>></li>
	    <?php $count++; ?>
	  <?php endwhile; ?>
	  </ol>
	  <div class="carousel-inner">
	  <!-- Wrapper for slides -->
	  <?php while( have_rows('slides') ): the_row(); ?>
	  	<?php $image = get_sub_field('image'); ?>
		  
		    <div class="item active">
		      <?php echo $img = wp_get_attachment_image($photo['id'], $size='full', $icon = false, $attr=array('class'=>'img-responsive')); ?> 
		      <?php if (get_sub_field('caption')): ?>
		      <div class="carousel-caption">
		        <?php the_sub_field('caption'); ?>
		      </div>
		      <?php endif; ?>
		    </div>
	  <?php endwhile; ?>
	  </div>
	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-frontpage" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-frontpage" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
	<?php endif; ?>
	<?php the_content(); ?>
	<?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
<?php endwhile; ?>
