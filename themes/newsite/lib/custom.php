<?php
/**
 * Custom functions
 */

function roots() {

    //--retina.js----Automatically looks for retina images using name@2x.jpg syntax
    wp_enqueue_script('retina-js', '//cdnjs.cloudflare.com/ajax/libs/retina.js/1.3.0/retina.min.js', array(), null, true );

    //--unique fonts loading
    wp_enqueue_style( 'fonts', get_template_directory_uri() . '/assets/fonts/font.css' );

    //wp_register_script('isotope', get_template_directory_uri() . '/assets/js/vendor/isotope.pkgd.min.js', array(), null, false);  //if use isotope (google it)
    // wp_register_script('angular', get_template_directory_uri() . '/assets/js/vendor/angular.min.js', array(), null, false);
}
add_action('wp_enqueue_scripts', 'roots');



//========== gallery + lightbox plugin =======================================
function magnific_popup_files() {
    wp_enqueue_style( 'magnific', get_template_directory_uri() . '/assets/css/vendor/magnific-popup-gallery.css' );
    wp_enqueue_script( 'magnific', get_template_directory_uri() .'/assets/js/vendor/magnific-popup-gallery.min.js', array('jquery'), false, true );
}

add_action( 'run_gallery', 'magnific_popup_files' );


//========== full page slider (fullPage.js)  =================================
function fullpageslider() {
    wp_enqueue_style( 'fullpage', get_template_directory_uri() . '/assets/css/vendor/jquery.fullPage.css' );
    wp_enqueue_script( 'fullpage-slimscroll', get_template_directory_uri() .'/assets/js/vendor/jquery.slimscroll.min.js');
    wp_enqueue_script( 'fullpag-core', get_template_directory_uri() .'/assets/js/vendor/jquery.fullPage.min.js', array('jquery'));

    wp_enqueue_script( 'fullpage-helper','//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js');


}

add_action( 'run_fullpage', 'fullpageslider' );  //hooked to the run_fullpage action use --> do_action( 'run_fullpage' );

//========== Login form shortcode  ===========================================
//========== use the following syntax -- > [loginform redirect="http://my-redirect-url.com"]
function login_form_shortcode( $atts, $content = null ) {

    extract( shortcode_atts( array(
        'redirect' => ''
    ), $atts ) );

    $form = '';
    if (!is_user_logged_in()) {
        if($redirect) {
            $redirect_url = $redirect;
        } else {
            $redirect_url = get_permalink();
        }
        $form = wp_login_form(array('echo' => false, 'redirect' => $redirect_url ));
    }
    return $form;
}
add_shortcode('loginform', 'login_form_shortcode');



// Remove WooCommerce Updater
remove_action('admin_notices', 'woothemes_updater_notice');

//========= Footer menu =======================================================
function register_footer_menu() {
    register_nav_menu('footer-menu',__( 'Footer Menu' ));
}
add_action( 'init', 'register_footer_menu' );

//==========FIX The double nav items =======================================
//The code below finds the menu item with the class "[CPT]-menu-item" and adds another “current_page_parent” class to it.
// Furthermore, it removes the “current_page_parent” from the blog menu item, if this is present.
// Via http://vayu.dk/highlighting-wp_nav_menu-ancestor-children-custom-post-types/

add_filter('nav_menu_css_class', 'current_type_nav_class', 10, 2);
function current_type_nav_class($classes, $item) {
    // Get post_type for this post
    $post_type = get_query_var('post_type');

    // Removes current_page_parent class from blog menu item
    if ( get_post_type() == $post_type )
        $classes = array_filter($classes, "get_current_value" );

    // Go to Menus and add a menu class named: {custom-post-type}-menu-item
    // This adds a current_page_parent class to the parent menu item
    if( in_array( $post_type.'-menu-item', $classes ) )
        array_push($classes, 'current_page_parent');

    return $classes;
}
function get_current_value( $element ) {
    return ( $element != "current_page_parent" );
}